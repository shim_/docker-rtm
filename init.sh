#!/bin/bash

BINARYDIR=/usr/local/rtm
BINARY=$BINARYDIR/bin/rtm
INSTALLER="/cache/install-$RTM_VERSION.sh"

function report_loop {
 while true; do
  $BINARY > /dev/null 2> /dev/null
  sleep $RTM_REPORT_INTERVAL
 done
}

function disable_command {
 rm -f $1
 echo -e '#!/bin/sh\n\n/bin/true' > $1
 chmod +x $1
 echo "Disabled $1"
}

function apply_restrictions {
 if [ ! -z "$RTM_HIDE_PROCESSES" ]; then
  disable_command "/bin/ps"
 fi

 if [ ! -z "$RTM_HIDE_CONNECTIONS" ]; then
  disable_command "/bin/netstat"
 fi
}

function fetch_installer {
 curl "$RTM_DOWNLOAD_URL$RTM_VERSION.sh" > $INSTALLER
 CHECKSUM=$(sha256sum $INSTALLER)
 if [ "$CHECKSUM" == "$RTM_INSTALLER_CHECKSUM  $INSTALLER" ]; then
  sed -i 's/df -li/df -i/g' $INSTALLER
  sed -i 's/df -l/df/g' $INSTALLER
  chmod +x $INSTALLER
  $INSTALLER
 else
  echo Checksum mismatch!
  exit 1
 fi
}

apply_restrictions

if [[ -d "$BINARYDIR" && -f $BINARY ]]; then 
 report_loop
else
 if [ -f "$INSTALLER" ]; then 
  chmod +x $INSTALLER
  $INSTALLER
 else
  fetch_installer
 fi
 report_loop
fi
