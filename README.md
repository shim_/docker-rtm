Usage:

docker run --privileged -v /proc:/proc -v /dev:/dev shimun/ovh-rtm

Options:

INTERVAL: how often the report should be sent, default 60
